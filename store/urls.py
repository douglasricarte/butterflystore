"""store URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from buterflystore import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns, static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home),
    path('home', views.home),
    path('NovoProduto', views.NovoProduto),
    path('novo', views.novo),
    path('amandaaraujo', views.admin),
    path('entrar',views.entrar),
    path('loginAdmin',views.LoginAdmin),
    path('Cliente', views.Cliente),
    path('NovoCliente',views.NovoCliente),
    path('AdicionarPedidoCarrinho/<id>', views.AdicionarPedidoCarrinho),
    path('LoginCliente', views.LoginCliente),
    path('AutenticarCliente', views.AutenticarCliente),
    path('Sair', views.Sair),
    path('produto/AdicionarPedidoCarrinho/<id>', views.AdicionarPedidoCarrinho),
    path('produto/<id>',views.ProdutoUnico),
    path('Excluir/<id>',views.Excluir),
    path('Editar/<id>',views.editar),
    path('Editar/Salvar/<id>', views.Salvar),
    path('Salvar/<id>', views.Salvar),
    path('VisualizarCarrinho', views.VisualizarCarrinho),
    path('RemoverCarrinho/<IdPedido>/<IdCarrinho>', views.RemoverCarrinho),
    path('Compra/<id>', views.compra),
    path('VisualizarCompras', views.VisualizarCompras),
    path('ComprasAdmin', views.ComprasAdmin),
    path('Produtos', views.Produtos),
    path('quantidade/<id>/<sinal>', views.quantidade),
    path('compraprodutos/<id>',views.compraprodutos),
    path('adminProdutos', views.adminProdutos),
    path('BuscaID', views.BuscaID),
    path('BuscaNome',views.BuscaNome),
    path('Buscar', views.Buscar),
    path('Missão', views.Missão),
    path('filtro/<tipo>', views.filtroTipo),
    path('filtros/<categoria>', views.filtroCategoria),

]
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)