from django.contrib import admin
from .models import Produto ,Pedido, Carrinho,Compra
# Register your models here.

admin.site.register(Produto)
admin.site.register(Pedido)
admin.site.register(Carrinho)
admin.site.register(Compra)
