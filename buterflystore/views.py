from django.shortcuts import render, redirect
import base64
from .models import Produto ,Carrinho, Compra, Pedido
from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth.models import User


#Create your views here.
from django.views.decorators.csrf import csrf_protect

def home(request):
    produtos = Produto.objects.filter(Vintrine = True)
    return render(request, 'home.html',{'produtos': produtos})
def Produtos(request):
    produtos = Produto.objects.all()
    return render(request, 'home.html', {'produtos': produtos})

def entrar(request):
    return render(request,'login.html')

@csrf_protect
@login_required(login_url='/entrar')
def admin(request):
    admin = User.objects.filter(username = request.user).values()
    if(admin[0]['is_staff'] == True):
        compra = Compra.objects.all().filter().order_by('hora').reverse()
        return render(request, 'admin.html', {'compras': compra})
    else:
        return redirect(home)

@csrf_protect
@login_required(login_url='/entrar')
def adminProdutos(request):
    admin = User.objects.filter(username = request.user).values()
    if(admin[0]['is_staff'] == True):
        produtos = Produto.objects.all()
        return render(request,'produtosAdm.html',{'produtos':produtos})
    else:
        return redirect(home)
def BuscaID(request):
    compra = Compra.objects.all().filter(id = request.POST.get('id'))
    return render(request, 'AdmCompras.html', {'compras': compra})
def BuscaNome(request):
    user = User.objects.filter(username = request.POST.get('nome')).values()
    compra = Compra.objects.filter(dono = user[0]['id'])
    return render(request, 'AdmCompras.html', {'compras': compra})

def novo(request):
    return render(request, 'form.html')

@login_required(login_url='/entrar')
@csrf_protect
def NovoProduto(request):
    #imagem01
    if request.FILES.get('img'):
        image = request.FILES['img']
        aux =request.FILES.get('img').name
        image_string = base64.b64encode(image.read())
        img = aux.split(".")[-1]
        img = "data:image/"+ img + ";base64," + image_string.decode('utf-8')
    else:
        img = None
    # imagem02
    if request.FILES.get('img1'):
        image = request.FILES['img1']
        aux = request.FILES.get('img1').name
        image_string = base64.b64encode(image.read())
        img2 = aux.split(".")[-1]
        img2 = "data:image/" + img2 + ";base64," + image_string.decode('utf-8')
    else:
        img2= None
    # imagem03
    if request.FILES.get('img2'):
        image = request.FILES['img2']
        aux = request.FILES.get('img2').name
        image_string = base64.b64encode(image.read())
        img3 = aux.split(".")[-1]
        img3 = "data:image/" + img3 + ";base64," + image_string.decode('utf-8')
    else:img3 = None
    preco = request.POST.get('preco')
    nome  = request.POST.get('nome')
    descricao = request.POST.get('descricao')
    disponibilidade = request.POST.get('disponibilidade')
    vitrine = request.POST.get('vitrine')
    tipo = request.POST.get('Tipo')
    categoria = request.POST.get('Categoria')
    produto = Produto.objects.create(nome = nome,preco = preco,descricao=descricao,vitrine=img,img2=img2,img3=img3,disponibilidade=disponibilidade, Vintrine = vitrine, tipo = tipo, categoria = categoria)
    return redirect(adminProdutos)

@csrf_protect
def LoginAdmin(request):
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user, backend=None)
            return redirect(admin)
        else:
            messages.error(request, " Usuario ou Senha invalida, tente novamente!")
            return redirect(entrar)
    else:
        return redirect(admin)
def Cliente(request):
    return  render(request,"NovoCliente.html")

def NovoCliente(request):
    try:
        user = User.objects.create_user(username=request.POST.get('nome'),email=request.POST.get('name'),password="123456")
        user.save()
    except:
        messages.error(request,"Username já está sendo utilizado!")
        return redirect(Cliente)
    return redirect(home)
@login_required(login_url='/LoginCliente')
def AdicionarPedidoCarrinho(request, id):
    dono = request.user
    pedido = Pedido.objects.create(IdProduto= id, comprador = dono, quantidade = request.POST.get('qtd'))
    pedidoCarrinho = Pedido.objects.filter(comprador=dono,status = True).values()
    produto = Produto.objects.filter(id =id).values()
    carrinho = Carrinho.objects.filter(dono=dono).values()
    num = -1
    for a in carrinho:
        num = num +1
        if (carrinho.exists() and (carrinho[num]['status'] == True)):
            if(carrinho[num]['IdPedido1'] == None):
                carrinho.update(IdPedido1 = pedidoCarrinho[0]['id'])
                valor = (carrinho[0]['ValorTotal'] + pedidoCarrinho[0]['quantidade'] * produto[0]['preco'])
                carrinhoAtualizarValor = Carrinho.objects.filter(dono=dono, status=True).values()
                carrinhoAtualizarValor.update(ValorTotal=valor)
                pedidoCarrinho.update(status = False)
            elif(carrinho[num]['IdPedido2'] == None):
                carrinho.update(IdPedido2=pedidoCarrinho[0]['id'])
                valor = (carrinho[0]['ValorTotal'] + pedidoCarrinho[0]['quantidade'] * produto[0]['preco'])
                carrinhoAtualizarValor = Carrinho.objects.filter(dono=dono, status=True).values()
                carrinhoAtualizarValor.update(ValorTotal=valor)
                pedidoCarrinho.update(status=False)
            elif (carrinho[num]['IdPedido3'] == None):
                carrinho.update(IdPedido3=pedidoCarrinho[0]['id'])
                valor = (carrinho[0]['ValorTotal'] + pedidoCarrinho[0]['quantidade'] * produto[0]['preco'])
                carrinhoAtualizarValor = Carrinho.objects.filter(dono=dono, status=True).values()
                carrinhoAtualizarValor.update(ValorTotal=valor)
                pedidoCarrinho.update(status=False)
            elif (carrinho[num]['IdPedido4'] == None):
                carrinho.update(IdPedido4=pedidoCarrinho[0]['id'])
                valor = (carrinho[0]['ValorTotal'] + pedidoCarrinho[0]['quantidade'] * produto[0]['preco'])
                carrinhoAtualizarValor = Carrinho.objects.filter(dono=dono, status=True).values()
                carrinhoAtualizarValor.update(ValorTotal = valor)
                pedidoCarrinho.update(status=False)
            elif (carrinho[num]['IdPedido5'] == None):
                carrinho.update(IdPedido5=pedidoCarrinho[0]['id'])
                valor = (carrinho[0]['ValorTotal'] + pedidoCarrinho[0]['quantidade'] * produto[0]['preco'])
                carrinhoAtualizarValor = Carrinho.objects.filter(dono=dono, status=True).values()
                carrinhoAtualizarValor.update(ValorTotal=valor)
                pedidoCarrinho.update(status=False)
            elif (carrinho[num]['IdPedido6'] == None):
                carrinho.update(IdPedido6=pedidoCarrinho[0]['id'])
                valor = (carrinho[0]['ValorTotal'] + pedidoCarrinho[0]['quantidade'] * produto[0]['preco'])
                carrinhoAtualizarValor = Carrinho.objects.filter(dono=dono, status=True).values()
                carrinhoAtualizarValor.update(ValorTotal=valor)
                pedidoCarrinho.update(status=False)
            elif (carrinho[num]['IdPedido7'] == None):
                carrinho.update(IdPedido7=pedidoCarrinho[0]['id'])
                valor = (carrinho[0]['ValorTotal'] + pedidoCarrinho[0]['quantidade'] * produto[0]['preco'])
                carrinhoAtualizarValor = Carrinho.objects.filter(dono=dono, status=True).values()
                carrinhoAtualizarValor.update(ValorTotal=valor)
                pedidoCarrinho.update(status=False)
            elif (carrinho[num]['IdPedido8'] == None):
                carrinho.update(IdPedido8=pedidoCarrinho[0]['id'])
                valor = (carrinho[0]['ValorTotal'] + pedidoCarrinho[0]['quantidade'] * produto[0]['preco'])
                carrinhoAtualizarValor = Carrinho.objects.filter(dono=dono, status=True).values()
                carrinhoAtualizarValor.update(ValorTotal=valor)
                pedidoCarrinho.update(status=False)
            elif (carrinho[num]['IdPedido9'] == None):
                carrinho.update(IdPedido9=pedidoCarrinho[0]['id'])
                valor = (carrinho[0]['ValorTotal'] + pedidoCarrinho[0]['quantidade'] * produto[0]['preco'])
                carrinhoAtualizarValor = Carrinho.objects.filter(dono=dono, status=True).values()
                carrinhoAtualizarValor.update(ValorTotal=valor)
                pedidoCarrinho.update(status=False)
            elif (carrinho[num]['IdPedido10'] == None):
                carrinho.update(IdPedido10=pedidoCarrinho[0]['id'])
                valor = (carrinho[0]['ValorTotal'] + pedidoCarrinho[0]['quantidade'] * produto[0]['preco'])
                carrinhoAtualizarValor = Carrinho.objects.filter(dono=dono, status=True).values()
                carrinhoAtualizarValor.update(ValorTotal=valor)
                pedidoCarrinho.update(status=False)
            else:
                print("Carrinho cheio")

    if( carrinho.exists() and carrinho[num]['status'] == False):
        NovoCarrinho = Carrinho.objects.create(dono=dono)
        carrinhoAtualizar = Carrinho.objects.filter(dono=dono, status=True)
        carrinhoAtualizarValor = Carrinho.objects.filter(dono=dono, status=True).values()
        carrinhoAtualizar.update(IdPedido1=pedidoCarrinho[0]['id'])
        valor = (carrinhoAtualizarValor[0]['ValorTotal'] + pedidoCarrinho[0]['quantidade'] * produto[0]['preco'])
        carrinhoAtualizar.update(ValorTotal = valor)
        pedidoCarrinho.update(status=False)
    if carrinho.exists() == False:
        NovoCarrinho = Carrinho.objects.create(dono = dono)
        carrinhoAtualizar = Carrinho.objects.filter(dono = dono, status = True)
        carrinhoAtualizarValor = Carrinho.objects.filter(dono=dono, status=True).values()
        carrinhoAtualizar.update(IdPedido1=pedidoCarrinho[0]['id'])
        valor = (carrinhoAtualizarValor[0]['ValorTotal'] + pedidoCarrinho[0]['quantidade'] * produto[0]['preco'])
        carrinhoAtualizar.update(ValorTotal=valor)
        pedidoCarrinho.update(status=False)

    return redirect(home)

@login_required(login_url='/LoginCliente')
def VisualizarCarrinho(request):
    aux = request.user
    car = Carrinho.objects.filter(dono=aux, status = True).values()
    car1 = Carrinho.objects.filter(dono=aux, status = True).values()
    pedidos = {}
    valorTotal = 0
    pedido1 = None
    pedido2 = None
    pedido3 = None
    pedido4 = None
    pedido5 = None
    pedido6 = None
    pedido7 = None
    pedido8 = None
    pedido9 = None
    pedido10 = None
    if car.exists() and (car[0]['status'] == True):
        if (car[0]['IdPedido1'] != None):
            pedido = Pedido.objects.filter(id = car[0]['IdPedido1']).values()
            #pedido1 = Pedido.objects.filter(id = car[0]['IdPedido1'])
            produto = Produto.objects.filter(id = pedido[0]['IdProduto']).values()
            aux = pedido[0]['quantidade'] * produto[0]['preco']
            valorTotal = valorTotal + aux
            pedido1 = {'IdPedido1': produto, 'quantidade':pedido[0]['quantidade'], 'idpedido': pedido[0]['id']}
        if (car[0]['IdPedido2'] != None):
            pedido = Pedido.objects.filter(id = car[0]['IdPedido2']).values()
            produto = Produto.objects.filter(id=pedido[0]['IdProduto']).values()
            aux = pedido[0]['quantidade'] * produto[0]['preco']
            valorTotal = valorTotal + aux
            pedido2 = {'IdPedido2': produto, 'quantidade':pedido[0]['quantidade'], 'idpedido': pedido[0]['id']}
        if (car[0]['IdPedido3'] != None):
            pedido = Pedido.objects.filter(id = car[0]['IdPedido3']).values()
            produto = Produto.objects.filter(id=pedido[0]['IdProduto']).values()
            aux = pedido[0]['quantidade'] * produto[0]['preco']
            valorTotal = valorTotal + aux
            pedido3 = {'IdPedido3': produto, 'quantidade':pedido[0]['quantidade'], 'idpedido': pedido[0]['id']}
        if (car[0]['IdPedido4'] != None):
            pedido = Pedido.objects.filter(id = car[0]['IdPedido4']).values()
            produto = Produto.objects.filter(id=pedido[0]['IdProduto']).values()
            aux = pedido[0]['quantidade'] * produto[0]['preco']
            valorTotal = valorTotal + aux
            pedido4 = {'IdPedido4': produto, 'quantidade':pedido[0]['quantidade'], 'idpedido': pedido[0]['id']}
        if (car[0]['IdPedido5'] != None):
            pedido = Pedido.objects.filter(id = car[0]['IdPedido5']).values()
            produto = Produto.objects.filter(id=pedido[0]['IdProduto']).values()
            aux = pedido[0]['quantidade'] * produto[0]['preco']
            valorTotal = valorTotal + aux
            pedido5 = {'IdPedido5': produto, 'quantidade':pedido[0]['quantidade'], 'idpedido': pedido[0]['id']}
        if (car[0]['IdPedido6'] != None):
            pedido = Pedido.objects.filter(id = car[0]['IdPedido6']).values()
            produto = Produto.objects.filter(id=pedido[0]['IdProduto']).values()
            aux = pedido[0]['quantidade'] * produto[0]['preco']
            valorTotal = valorTotal + aux
            pedido6 = {'IdPedido6': produto, 'quantidade':pedido[0]['quantidade'], 'idpedido': pedido[0]['id']}
        if (car[0]['IdPedido7'] != None):
            pedido = Pedido.objects.filter(id = car[0]['IdPedido7']).values()
            produto = Produto.objects.filter(id=pedido[0]['IdProduto']).values()
            aux = pedido[0]['quantidade'] * produto[0]['preco']
            valorTotal = valorTotal + aux
            pedido7 = {'IdPedido7': produto, 'quantidade':pedido[0]['quantidade'], 'idpedido': pedido[0]['id']}
        if (car[0]['IdPedido8'] != None):
            pedido = Pedido.objects.filter(id = car[0]['IdPedido8']).values()
            produto = Produto.objects.filter(id=pedido[0]['IdProduto']).values()
            aux = pedido[0]['quantidade'] * produto[0]['preco']
            valorTotal = valorTotal + aux
            pedido8 = {'IdPedido8': produto, 'quantidade':pedido[0]['quantidade'], 'idpedido': pedido[0]['id']}
        if (car[0]['IdPedido9'] != None):
            pedido = Pedido.objects.filter(id = car[0]['IdPedido9']).values()
            produto = Produto.objects.filter(id=pedido[0]['IdProduto']).values()
            aux = pedido[0]['quantidade'] * produto[0]['preco']
            valorTotal = valorTotal + aux
            pedido9 = {'IdPedido9': produto, 'quantidade':pedido[0]['quantidade'], 'idpedido': pedido[0]['id']}
        if (car[0]['IdPedido10'] != None):
            pedido = Pedido.objects.filter(id=car[0]['IdPedido10']).values()
            produto = Produto.objects.filter(id=pedido[0]['IdProduto']).values()
            aux = pedido[0]['quantidade'] * produto[0]['preco']
            valorTotal = valorTotal + aux
            pedido10 = {'IdPedido10': produto, 'quantidade':pedido[0]['quantidade'], 'idpedido': pedido[0]['id']}
        id = car[0]['id']
        pedidos = {'id':id,'p1': pedido1,'p2':pedido2,'p3':pedido3,'p4':pedido4,'p5':pedido5,'p6':pedido6,'p7':pedido7,'p8':pedido8,'p9':pedido9,'p10': pedido10, 'valorTotal':valorTotal}
    return render(request, 'Carrinho.html',{"pedidos" : pedidos})

def LoginCliente(request):
    return render(request, 'LoginCliente.html')

def AutenticarCliente(request):
    if request.POST:
        username = request.POST.get('nome')
        password = "123456"
        user = authenticate(username=username, password = password)
        if user is not None:
            login(request, user, backend=None)
            return redirect(home)
        else:
            messages.error(request, "Usuario invalido, tente novamente!")
            return redirect(LoginCliente)
    else:
        return redirect(admin)
def Sair(request):
    logout(request)
    return redirect(home)
def editar(request, id):
    produto = Produto.objects.get(id=id)
    return render(request, 'Editar.html', {'produto': produto})

def ProdutoUnico(request, id):
    produto = Produto.objects.get(id=id)
    return render(request, 'produto.html', {'produto': produto})

def Excluir(request, id):
    produto = Produto.objects.filter(id = id)
    produto.delete()
    return redirect(adminProdutos)
def Salvar(request, id):
    produto = Produto.objects.filter(id = id)
    produto.update(nome = request.POST.get('nome'), preco = request.POST.get('preco'), disponibilidade = request.POST.get('disponibilidade'), Vintrine = request.POST.get('vitrine'), categoria = request.POST.get('Categoria'), tipo = request.POST.get('Tipo'))
    if request.POST.get('descricao'):
        produto.update(descricao = request.POST.get('descricao'))
        # imagem01
    if request.FILES.get('img'):
        image = request.FILES['img']
        aux = request.FILES.get('img').name
        image_string = base64.b64encode(image.read())
        img = aux.split(".")[-1]
        img = "data:image/" + img + ";base64," + image_string.decode('utf-8')
        produto.update(vitrine = img)
    else:
        img = None
        # imagem02
    if request.FILES.get('img1'):
        image = request.FILES['img1']
        aux = request.FILES.get('img1').name
        image_string = base64.b64encode(image.read())
        img2 = aux.split(".")[-1]
        img2 = "data:image/" + img2 + ";base64," + image_string.decode('utf-8')
        print("errou")
        produto.update(img2 = img2)
    else:
         img2 = None
        # imagem03
    if request.FILES.get('img2'):
        image = request.FILES['img2']
        aux = request.FILES.get('img2').name
        image_string = base64.b64encode(image.read())
        img3 = aux.split(".")[-1]
        img3 = "data:image/" + img3 + ";base64," + image_string.decode('utf-8')
        produto.update(img3 = img3)
    else:
        img3 = None
    return redirect(adminProdutos)

def RemoverCarrinho(request, IdPedido, IdCarrinho):
    car =  Carrinho.objects.filter(id = IdCarrinho).values()
    aux = int(IdPedido)
    if (car[0]['IdPedido1'] != None):
        aux1 = int(car[0]['IdPedido1'])
        if (aux == aux1):
            car.update(IdPedido1 = None)
            pedido = Pedido.objects.filter(id = IdPedido)
            pedido.delete()
    if (car[0]['IdPedido2'] != None):
        aux1 = int(car[0]['IdPedido2'])
        if (aux == aux1):
            car.update(IdPedido2= None)
            pedido = Pedido.objects.filter(id=IdPedido)
            pedido.delete()
    if (car[0]['IdPedido3'] != None):
        aux1 = int(car[0]['IdPedido3'])
        if (aux == aux1):
            car.update(IdPedido3=None)
            pedido = Pedido.objects.filter(id=IdPedido)
            pedido.delete()
    if (car[0]['IdPedido4'] != None):
        aux1 = int(car[0]['IdPedido4'])
        if (aux == aux1):
            car.update(IdPedido4=None)
            pedido = Pedido.objects.filter(id=IdPedido)
            pedido.delete()
    if (car[0]['IdPedido5'] != None):
        aux1 = int(car[0]['IdPedido5'])
        if (aux == aux1):
            car.update(IdPedido5=None)
            pedido = Pedido.objects.filter(id=IdPedido)
            pedido.delete()
    if (car[0]['IdPedido6'] != None):
        aux1 = int(car[0]['IdPedido6'])
        if (aux == aux1):
            car.update(IdPedido6=None)
            pedido = Pedido.objects.filter(id=IdPedido)
            pedido.delete()
    if (car[0]['IdPedido7'] != None):
        aux1 = int(car[0]['IdPedido7'])
        if (aux == aux1):
            car.update(IdPedido7=None)
            pedido = Pedido.objects.filter(id=IdPedido)
            pedido.delete()
    if (car[0]['IdPedido8'] != None):
        aux1 = int(car[0]['IdPedido8'])
        if (aux == aux1):
            car.update(IdPedido8=None)
            pedido = Pedido.objects.filter(id=IdPedido)
            pedido.delete()
    if (car[0]['IdPedido9'] != None):
        aux1 = int(car[0]['IdPedido9'])
        if (aux == aux1):
            car.update(IdPedido9=None)
            pedido = Pedido.objects.filter(id=IdPedido)
            pedido.delete()
    if (car[0]['IdPedido10'] != None):
        aux1 = int(car[0]['IdPedido10'])
        if (aux == aux1):
            car.update(IdPedido10=None)
            pedido = Pedido.objects.filter(id=IdPedido)
            pedido.delete()

    return redirect(VisualizarCarrinho)
def compra(request, id):
    carrinho = Carrinho.objects.get(id = id)
    carrinhoFechado = Carrinho.objects.filter(id = id)
    carrinhoFechado.update(status = False)
    comprafeita = Compra.objects.create(carrinho = carrinho, dono = request.user)
    return redirect(VisualizarCompras)
@login_required(login_url='/LoginCliente')
def VisualizarCompras(request):
    compra = Compra.objects.filter(dono = request.user)
    return render(request, 'VisualizarCompras.html', {'compras': compra})

def ComprasAdmin(request):
    compra = Compra.objects.all().filter().order_by('hora').reverse()
    return render(request, 'AdmCompras.html', {'compras': compra})

def quantidade(request, id, sinal):
    pedido = Pedido.objects.filter(id = id)
    pedidoValor = Pedido.objects.filter(id=id).values()
    quantidade = pedidoValor[0]['quantidade']
    if (sinal == "+"):
        quantidade = quantidade +1
        pedido.update(quantidade = quantidade)
    if (sinal == "-" and quantidade>1):
        quantidade = quantidade - 1
        pedido.update(quantidade = quantidade)
        if quantidade == 0:
            pedido.delete()

    return redirect(VisualizarCarrinho)
@login_required(login_url='/entrar')
def compraprodutos(request, id):
    compra = Compra.objects.get(id=id)
    idc = compra.carrinho.id
    print(idc)
    carrinho = Carrinho.objects.filter(id = idc).values()
    print(carrinho)
    aux = request.user
    pedidos = {}
    valorTotal = 0
    pedido1 = None
    pedido2 = None
    pedido3 = None
    pedido4 = None
    pedido5 = None
    pedido6 = None
    pedido7 = None
    pedido8 = None
    pedido9 = None
    pedido10 = None
    if carrinho.exists():
        if (carrinho[0]['IdPedido1'] != None):
            pedido = Pedido.objects.filter(id=carrinho[0]['IdPedido1']).values()
            # pedido1 = Pedido.objects.filter(id = car[0]['IdPedido1'])
            produto = Produto.objects.filter(id=pedido[0]['IdProduto']).values()
            pedido1 = {'IdPedido1': produto, 'quantidade': pedido[0]['quantidade'], 'idpedido': pedido[0]['id']}
        if (carrinho[0]['IdPedido2'] != None):
            pedido = Pedido.objects.filter(id=carrinho[0]['IdPedido2']).values()
            produto = Produto.objects.filter(id=pedido[0]['IdProduto']).values()
            pedido2 = {'IdPedido2': produto, 'quantidade': pedido[0]['quantidade'], 'idpedido': pedido[0]['id']}
        if (carrinho[0]['IdPedido3'] != None):
            pedido = Pedido.objects.filter(id=carrinho[0]['IdPedido3']).values()
            produto = Produto.objects.filter(id=pedido[0]['IdProduto']).values()
            pedido3 = {'IdPedido3': produto, 'quantidade': pedido[0]['quantidade'], 'idpedido': pedido[0]['id']}
        if (carrinho[0]['IdPedido4'] != None):
            pedido = Pedido.objects.filter(id=carrinho[0]['IdPedido4']).values()
            produto = Produto.objects.filter(id=pedido[0]['IdProduto']).values()
            pedido4 = {'IdPedido4': produto, 'quantidade': pedido[0]['quantidade'], 'idpedido': pedido[0]['id']}
        if (carrinho[0]['IdPedido5'] != None):
            pedido = Pedido.objects.filter(id=carrinho[0]['IdPedido5']).values()
            produto = Produto.objects.filter(id=pedido[0]['IdProduto']).values()
            pedido5 = {'IdPedido5': produto, 'quantidade': pedido[0]['quantidade'], 'idpedido': pedido[0]['id']}
        if (carrinho[0]['IdPedido6'] != None):
            pedido = Pedido.objects.filter(id=carrinho[0]['IdPedido6']).values()
            produto = Produto.objects.filter(id=pedido[0]['IdProduto']).values()
            pedido6 = {'IdPedido6': produto, 'quantidade': pedido[0]['quantidade'], 'idpedido': pedido[0]['id']}
        if (carrinho[0]['IdPedido7'] != None):
            pedido = Pedido.objects.filter(id=carrinho[0]['IdPedido7']).values()
            produto = Produto.objects.filter(id=pedido[0]['IdProduto']).values()
            pedido7 = {'IdPedido7': produto, 'quantidade': pedido[0]['quantidade'], 'idpedido': pedido[0]['id']}
        if (carrinho[0]['IdPedido8'] != None):
            pedido = Pedido.objects.filter(id=carrinho[0]['IdPedido8']).values()
            produto = Produto.objects.filter(id=pedido[0]['IdProduto']).values()
            pedido8 = {'IdPedido8': produto, 'quantidade': pedido[0]['quantidade'], 'idpedido': pedido[0]['id']}
        if (carrinho[0]['IdPedido9'] != None):
            pedido = Pedido.objects.filter(id=carrinho[0]['IdPedido9']).values()
            produto = Produto.objects.filter(id=pedido[0]['IdProduto']).values()
            pedido9 = {'IdPedido9': produto, 'quantidade': pedido[0]['quantidade'], 'idpedido': pedido[0]['id']}
        if (carrinho[0]['IdPedido10'] != None):
            pedido = Pedido.objects.filter(id=carrinho[0]['IdPedido10']).values()
            produto = Produto.objects.filter(id=pedido[0]['IdProduto']).values()
            pedido10 = {'IdPedido10': produto, 'quantidade': pedido[0]['quantidade'], 'idpedido': pedido[0]['id']}
        id = carrinho[0]['id']
        pedidos = {'id': id, 'p1': pedido1, 'p2': pedido2, 'p3': pedido3, 'p4': pedido4, 'p5': pedido5, 'p6': pedido6,
                   'p7': pedido7, 'p8': pedido8, 'p9': pedido9, 'p10': pedido10, 'valorTotal': valorTotal}
    return render(request, 'produtoscompra.html', {'pedidos': pedidos})

def Buscar(request):
    produtos = Produto.objects.filter(nome=request.POST.get('buscar'))
    produto2 = Produto.objects.filter(categoria=request.POST.get('buscar'))
    return render(request, 'home.html', {'produtos': produtos|produto2})
def Missão(request):
    return render( request, 'missão.html')
def filtroTipo(request, tipo):
    produtos = Produto.objects.filter(tipo = tipo)
    return render(request, 'home.html', {'produtos': produtos})
def filtroCategoria(request, categoria):
    produtos = Produto.objects.filter(categoria = categoria)
    return render(request, 'home.html', {'produtos': produtos})