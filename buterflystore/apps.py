from django.apps import AppConfig


class ButerflystoreConfig(AppConfig):
    name = 'buterflystore'
