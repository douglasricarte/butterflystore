from django.db import models
from django.contrib.auth.models import User
import jsonfield

# Create your models here.

class Produto(models.Model):
    nome = models.CharField(max_length=200)
    descricao = models.TextField()
    disponibilidade = models.BooleanField(default=True)
    preco = models.DecimalField (max_digits = 5, decimal_places = 2, null=False)
    vitrine = models.TextField(null=True)
    img2 = models.TextField(null=True)
    img3 = models.TextField(null=True)
    categoria = models.CharField(max_length=50, null=True)
    tipo = models.CharField(max_length=50, null=True)
    Vintrine = models.CharField(max_length=10 ,null=True)
    def __str__(self):
        return self.nome

class Pedido(models.Model):
    IdProduto = models.IntegerField()
    comprador = models.ForeignKey(User, on_delete=models.CASCADE)
    data = models.DateField(auto_now_add=True)
    quantidade = models.IntegerField(default=1)
    status = models.BooleanField(default=True)

class Carrinho(models.Model):
    dono = models.ForeignKey(User, on_delete=models.CASCADE)
    IdPedido1 = models.IntegerField(null=True, blank=True)
    IdPedido2 = models.IntegerField(null=True,blank=True)
    IdPedido3 = models.IntegerField(null=True,blank=True)
    IdPedido4 = models.IntegerField(null=True,blank=True)
    IdPedido5 = models.IntegerField(null=True,blank=True)
    IdPedido6 = models.IntegerField(null=True,blank=True)
    IdPedido7 = models.IntegerField(null=True,blank=True)
    IdPedido8 = models.IntegerField(null=True,blank=True)
    IdPedido9 = models.IntegerField(null=True,blank=True)
    IdPedido10 = models.IntegerField(null=True,blank=True)
    ValorTotal = models.DecimalField (max_digits = 5, decimal_places = 2, null=False, default=0)
    status = models.BooleanField(default=True,blank=True)
class Compra(models.Model):
    dono = models.ForeignKey(User, on_delete=models.CASCADE)
    data = models.DateField(auto_now_add =True, null=True)
    hora = models.DateTimeField(null=True, auto_now=True)
    carrinho = models.OneToOneField(Carrinho, on_delete=models.CASCADE)