# Generated by Django 3.0.8 on 2020-07-30 14:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('buterflystore', '0025_carrinho_valortotal'),
    ]

    operations = [
        migrations.AlterField(
            model_name='carrinho',
            name='ValorTotal',
            field=models.IntegerField(default=0, null=True),
        ),
    ]
