# Generated by Django 3.0.8 on 2020-07-28 00:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('buterflystore', '0003_produto'),
    ]

    operations = [
        migrations.AddField(
            model_name='produto',
            name='categoria',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
