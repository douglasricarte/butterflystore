# Generated by Django 3.0.8 on 2020-07-28 12:05

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('buterflystore', '0004_produto_categoria'),
    ]

    operations = [
        migrations.CreateModel(
            name='Pedido',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data', models.DateField(auto_now_add=True)),
                ('comprador', models.ManyToManyField(to=settings.AUTH_USER_MODEL)),
                ('produto', models.ManyToManyField(to='buterflystore.Produto')),
            ],
        ),
        migrations.CreateModel(
            name='Carrinho',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.BooleanField(default=True)),
                ('pedido', models.ManyToManyField(to='buterflystore.Pedido')),
            ],
        ),
    ]
